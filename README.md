"# Plot_And_Scroll_Images" 
Used to plot and scroll through images

    from Plot_Scroll_Images import plot_scroll_Image
    
    x = some_array
    x.shape = (25, 512, 512, 1)
    plot_scroll_Image(x)
